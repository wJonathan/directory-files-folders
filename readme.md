#Directory Files/Folders
v 1.0  

**Get_files.php**, is a class for displaying directory files and folders, you can also view a directory tree from a given directory.


##Usage

In the head of your HTML of PHP  
`require 'Get_files.php';`

Create a new instance of the class  
`$insta = new Get_files();`

Echo out the list that you want  
`echo $insta->directory_files('directory');`

##Features

Get all the files from a given directory.  
`directory_files('directory');`

Get all the folders from a given directory.  
`directory_folders('directory');`

Get all files and folders from a given directory.  
`directory('directory');`

Get a tree structure of all files and folders from given directory to the end of the tree.  
`directory_tree('directory');` 