<?php
/*
 * @author 		wJonathan
 * @class 		Get_files
 * @desc 		This class gets the files and folders from a directory.
 * @variables 	$c = directory, $f = file/folder, $ff = file/folder path
 */

Class Get_files{
	public function __construct(){
	}

	/*
	 * @author 	wJonathan
 	 * @method 	directory_files
 	 * @desc 	This class gets the files from a directory.
 	 * @param 	string $c (directory)
 	 * @return 	string List of files
 	 */
	public function directory_files($c){
		foreach(scandir($c) as $f){
			$ff = $c . '/' . $f;
			if(!is_dir($ff)){
				$list_items .= '<li><a href="'. $ff .'" >' . $f . '</a></li>'; 
			}
		}
		return '<ul>' . $list_items . '</ul>';
	}

	/*
	 * @author 	wJonathan
 	 * @method 	directory_folders
 	 * @desc 	This class gets the folders from a directory.
 	 * @param 	string $c
 	 * @return 	string List of folders
 	 */
	public function directory_folders($c){
		foreach(scandir($c) as $f){
			if(strpos($f, '.') === 0) continue;
			$ff = $c . '/' . $f;
			if(is_dir($ff)){
				$list_items .= '<li><a href="'. $ff .'" >' . $f . '</a></li>'; 
			}
		}
		return '<ul>' . $list_items . '</ul>';
	}

	/*
	 * @author 	wJonathan
 	 * @method 	directory
 	 * @desc 	This class gets the files and folders from a directory.
 	 * @param 	string $c
 	 * @return 	string List of files and folders
 	 */
	public function directory($c){
		foreach(scandir($c) as $f){
			if(strpos($f, '.') === 0) continue;
			$ff = $c . '/' . $f;
			$list_items .= '<li><a href="'. $ff .'" >' . $f . '</a></li>';
		}
		return '<ul>' . $list_items . '</ul>';
	}

	/*
	 * @author 	wJonathan
 	 * @method 	directory_tree
 	 * @desc 	This class gets the tree of files and folders from a directory.
 	 * @param 	string $c
 	 * @return 	string Lists of all files and subfolders from a directory
 	 */
	public function directory_tree($c){
		foreach(scandir($c) as $f){
			if(strpos($f, '.') === 0) continue;
			$ff = $c . '/' . $f;
			$list_items .= '<li><a href="'. $ff .'" >' . $f . '</a>';
			if(is_dir($ff)){
				$list_items .= $this->directory_tree($ff);
			}
			$list_items .= '</li>';
		}
		return '<ul>' . $list_items . '</ul>';
	}
}